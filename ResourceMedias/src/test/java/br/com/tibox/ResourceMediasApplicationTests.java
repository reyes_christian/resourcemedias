package br.com.tibox;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = ResourceMediasApplication.class)
public class ResourceMediasApplicationTests {

	@Test
	public void contextLoads() {
	}

}
