package br.com.tibox.entity;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;


public class ContratoConfiguracoes implements Serializable {

	private static final long serialVersionUID = -614032646665908786L;
    
    @SerializedName("client_id")
    private Long clientId;
    @SerializedName("velocidade_descarga")
    private Integer velocidadeDescarga;
    @SerializedName("tempo_requisicao")
    private Integer tempoRequisicao;
    @SerializedName("qtd_historico")
    private Integer qtdHistorico;
    @SerializedName("tipo_autenticacao")
    private Integer tipoAutenticacao;

    public Long getClientId() {
        return clientId;
    }

    public void setClientId(Long clientId) {
        this.clientId = clientId;
    }

    public Integer getVelocidadeDescarga() {
        return velocidadeDescarga;
    }

    public void setVelocidadeDescarga(Integer velocidadeDescarga) {
        this.velocidadeDescarga = velocidadeDescarga;
    }

    public Integer getTempoRequisicao() {
        return tempoRequisicao;
    }

    public void setTempoRequisicao(Integer tempoRequisicao) {
        this.tempoRequisicao = tempoRequisicao;
    }

    public Integer getQtdHistorico() {
        return qtdHistorico;
    }

    public void setQtdHistorico(Integer qtdHistorico) {
        this.qtdHistorico = qtdHistorico;
    }

    public Integer getTipoAutenticacao() {
        return tipoAutenticacao;
    }

    public void setTipoAutenticacao(Integer tipoAutenticacao) {
        this.tipoAutenticacao = tipoAutenticacao;
    }

    @Override
   	public String toString() {
   		return "ContratoConfiguracoes [clientId=" + clientId + ", velocidadeDescarga=" + velocidadeDescarga
   				+ ", tempoRequisicao=" + tempoRequisicao + ", qtdHistorico=" + qtdHistorico + ", tipoAutenticacao="
   				+ tipoAutenticacao + "]";
   	}

}
