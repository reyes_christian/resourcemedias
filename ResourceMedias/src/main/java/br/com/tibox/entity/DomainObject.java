package br.com.tibox.entity;

import java.io.Serializable;

public interface DomainObject extends Serializable {

    public String getKey();

    public String getObjectKey();
}
