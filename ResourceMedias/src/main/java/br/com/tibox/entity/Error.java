package br.com.tibox.entity;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;

public class Error implements Serializable {

    private static final long serialVersionUID = 8727602238400650346L;

    /**
     * Se o error for igual a zero significa que tudo esta ok, maior que zero e
     * porque deu erro.
     */
    @SerializedName("id_error")
    private long error;

    public Error(long error) {
        this.error = error;
    }

    public long getError() {
        return error;
    }

    public void setError(long error) {
        this.error = error;
    }

	@Override
	public String toString() {
		return "Error [error=" + error + "]";
	}
}
