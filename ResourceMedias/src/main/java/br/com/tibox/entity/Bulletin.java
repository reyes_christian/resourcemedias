package br.com.tibox.entity;

import java.util.HashSet;

import com.google.gson.annotations.SerializedName;

public class Bulletin implements DomainObject {

    private static final long serialVersionUID = 6896444537978639077L;
    
    public static final String OBJECT_KEY = "BULLETIN";

    @SerializedName("bulletin_id")
    private Long bulletinId;
    @SerializedName("resource_id")
    private Long resourceId;
    private String title, w, h, timestamp;

    public String getTimestamp() {
        if (Long.valueOf(timestamp) < 99999999999l) {
            return Long.toString(Long.valueOf(timestamp) * 1000);
        }
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    private HashSet<Long> dependencies;
    //Determina se e super alerta ou normal
    private String style;

    public String getStyle() {
        return style;
    }

    public void setStyle(String style) {
        this.style = style;
    }

    public String getW() {
        return w;
    }

    public void setW(Integer w) {
        this.w = Integer.toString(w);
    }

    public String getH() {
        return h;
    }

    public void setH(int h) {
        this.h = Integer.toString(h);
    }

    public Long getBulletinId() {
        return bulletinId;
    }

    public void setBulletinId(Long bulletinId) {
        this.bulletinId = bulletinId;
    }

    public Long getResourceId() {
        return resourceId;
    }

    public void setResourceId(Long resourceId) {
        this.resourceId = resourceId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public HashSet<Long> getDependencies() {
        return dependencies;
    }

    public void setDependencies(HashSet<Long> dependencies) {
        this.dependencies = dependencies;
    }

	@Override
	public String toString() {
		return "Bulletin [bulletinId=" + bulletinId + ", resourceId=" + resourceId + ", title=" + title + ", timestamp="
				+ timestamp + ", dependencies=" + dependencies + "]";
	}

	@Override
	 public String getKey() {
	  return String.valueOf(getResourceId());
	 }

	 @Override
	 public String getObjectKey() {
	  return OBJECT_KEY;
	 }
}
