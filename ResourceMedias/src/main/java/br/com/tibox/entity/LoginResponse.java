package br.com.tibox.entity;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;

public class LoginResponse implements Serializable {

    private static final long serialVersionUID = 2006256184440158632L;

    @SerializedName("authenticated")
    private boolean authenticated;
    @SerializedName("user_id")
    private String userId;
    @SerializedName("token")
    private String token;

    public boolean isAuthenticated() {
        return authenticated;
    }

    public void setAuthenticated(boolean authenticated) {
        this.authenticated = authenticated;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
