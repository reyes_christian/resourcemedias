package br.com.tibox.entity;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;

public class ClienteRequest implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8728856247736035155L;
	
	@SerializedName("client_time")
	private Long clientTime;
	
	@SerializedName("config_revision")
	private Integer configRevision;
	
	@SerializedName("machine_id")
	private String machineId;
	
	@SerializedName("user_id")
	private String userId;
	
	@SerializedName("client_code")
	private Long clientCode;
	
	@SerializedName("mobile")
	private Boolean mobile;
	
	@SerializedName("pagina")
	private Integer pagina;
	
	@SerializedName("total_registros")
	private Integer totalRegistros;

	public Long getClientTime() {
		return clientTime;
	}

	public void setClientTime(Long clientTime) {
		this.clientTime = clientTime;
	}

	public Integer getConfigRevision() {
		return configRevision;
	}

	public void setConfigRevision(Integer configRevision) {
		this.configRevision = configRevision;
	}

	public String getMachineId() {
		return machineId;
	}

	public void setMachineId(String machineId) {
		this.machineId = machineId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Long getClientCode() {
		return clientCode;
	}

	public void setClientCode(Long clientCode) {
		this.clientCode = clientCode;
	}

	public Boolean getMobile() {
		return mobile;
	}

	public void setMobile(Boolean mobile) {
		this.mobile = mobile;
	}

	public Integer getPagina() {
		return pagina;
	}

	public void setPagina(Integer pagina) {
		this.pagina = pagina;
	}

	public Integer getTotalRegistros() {
		return totalRegistros;
	}

	public void setTotalRegistros(Integer totalRegistros) {
		this.totalRegistros = totalRegistros;
	}
	

}
