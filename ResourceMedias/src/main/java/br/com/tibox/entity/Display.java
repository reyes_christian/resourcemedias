package br.com.tibox.entity;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;

public class Display implements Serializable{
	
	private static final long serialVersionUID = 7899387733983766863L;
	
    @SerializedName("bulletin_id")
    private Long bolletinId;
    
    private String start;
    private String end;
    private String title, w, h, url, style;

    public Long getBolletinId() {
        return bolletinId;
    }

    public void setBolletinId(Long bolletinId) {
        this.bolletinId = bolletinId;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getEnd() {
        return end;
    }

    public void setEnd(String end) {
        this.end = end;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getW() {
        return w;
    }

    public void setW(String w) {
        this.w = w;
    }

    public String getH() {
        return h;
    }

    public void setH(String h) {
        this.h = h;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
    
    public String getStyle() {
        return style;
    }

    public void setStyle(String style) {
        this.style = style;
    }
}
