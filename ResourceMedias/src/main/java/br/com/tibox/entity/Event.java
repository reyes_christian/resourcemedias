package br.com.tibox.entity;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;

public class Event implements Serializable {

    private static final long serialVersionUID = 1538804971798110366L;

    @SerializedName("bulletin_id")
    private long bulletinId;

    @SerializedName("machine_id")
    private String machineId;

    private String flavor;

    @SerializedName("user_id")
    private String userId;

    @SerializedName("client_id")
    private long clientCode;

    private String timestamp;

    @SerializedName("mobile")
    private boolean isMobile;

    public long getBulletinId() {
        return bulletinId;
    }

    public void setBulletinId(long bulletinId) {
        this.bulletinId = bulletinId;
    }

    public String getMachineId() {
        return machineId;
    }

    public void setMachineId(String machineId) {
        this.machineId = machineId;
    }

    public String getFlavor() {
        return flavor;
    }

    public void setFlavor(String flavor) {
        this.flavor = flavor;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public long getClientCode() {
        return clientCode;
    }

    public void setClientCode(long clientCode) {
        this.clientCode = clientCode;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public boolean isMobile() {
        return isMobile;
    }

    public void setMobile(boolean isMobile) {
        this.isMobile = isMobile;
    }

}
