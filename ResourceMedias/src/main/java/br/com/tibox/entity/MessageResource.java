package br.com.tibox.entity;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.SerializedName;

public class MessageResource implements DomainObject {

    private static final long serialVersionUID = 7262254158082852247L;
    
    public static final String MESSAGE_KEY = "MESSAGE_RESOURCE";
    
    private Integer Id;

    @SerializedName("message_type")
    private String messageType;

    private List<Resource> entries;

    public MessageResource() {
    }

    public MessageResource(String messageType) {
        this.messageType = messageType;
        entries = new ArrayList<Resource>();
    }

    public String getMessageType() {
        return messageType;
    }

    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }

    public List<Resource> getEntries() {
        return entries;
    }

    public void setEntries(List<Resource> entries) {
        this.entries = entries;
    }

    public static String getNovosAlertasKey(long cdCliente) {
        return cdCliente + "novosalertas:";
    }

    public static String getAlertasKey(long cdCliente) {
        return cdCliente + "alertas:";
    }
    
    public Integer getId() {
		return this.Id;
	}

    @Override
    public String getKey() {
        return String.valueOf(getId());
    }

    @Override
    public String getObjectKey() {
        return MESSAGE_KEY;
    }
}
