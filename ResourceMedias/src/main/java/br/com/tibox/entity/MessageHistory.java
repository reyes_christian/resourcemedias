package br.com.tibox.entity;

import java.io.Serializable;
import java.util.ArrayList;

import com.google.gson.annotations.SerializedName;

public class MessageHistory implements Serializable {

    private static final long serialVersionUID = 6447423680076253577L;

    @SerializedName("message_type")
    private String messageType;

    @SerializedName("bulletin_ids")
    private ArrayList<Long> bulletinIds;

    @SerializedName("bulletin_time_ids")
    private ArrayList<Historico> bulletinTimeIds;

    public MessageHistory() {
        bulletinIds = new ArrayList<Long>();
        bulletinTimeIds = new ArrayList<Historico>();
    }

    public MessageHistory(String messageType) {
        this.messageType = messageType;
        bulletinIds = new ArrayList<Long>();
        bulletinTimeIds = new ArrayList<Historico>();
    }

    public String getMessageType() {
        return messageType;
    }

    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }

    public ArrayList<Long> getBulletinIds() {
        return bulletinIds;
    }

    public void setBulletinIds(ArrayList<Long> bulletinIds) {
        this.bulletinIds = bulletinIds;
    }

    public static String getObjectKey(long cdCliente) {
        return cdCliente + "destinatario:";
    }

    public ArrayList<Historico> getBulletinTimeIds() {
        return bulletinTimeIds;
    }

    public void setBulletinTimeIds(ArrayList<Historico> bulletinTimeIds) {
        this.bulletinTimeIds = bulletinTimeIds;
    }


}
