package br.com.tibox.entity;

public class Video implements DomainObject{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8508302417946259263L;
	
	public static final String OBJECT_KEY = "IMAGE";
	
	private static final int IMPOSSIBLE_ID = -1;
	
	private Integer id = IMPOSSIBLE_ID;
	private String identifier;
    private String provider;
    private String title;
    private String description;

    
    public String getIdentifier() {
        return identifier;
    }

    public String getProvider() {
        return provider;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public String toString() {
        return "Video [identifier=" + identifier + ", provider=" + provider
                + ", title=" + title + "]";
    }
    
    public Integer getId() {
        return id;
    }


	@Override
	public String getKey() {
		return getTitle();
	}

	@Override
	public String getObjectKey() {
		return OBJECT_KEY;
	}

}
