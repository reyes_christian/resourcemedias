package br.com.tibox.entity;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;

public class ClientLogin implements Serializable {

    private static final long serialVersionUID = 2006257884940158632L;

    @SerializedName("machine_id")
    private String machineId;
    @SerializedName("password")
    private String password;
    @SerializedName("password_md5")
    private String passwordMD5;
    @SerializedName("salt")
    private String salt;
    @SerializedName("username")
    private String username;
    @SerializedName("client_code")
    private long clientCode;
    @SerializedName("mobile")
    private boolean isMobile;

    public String getMachineId() {
        return machineId;
    }

    public void setMachineId(String machineId) {
        this.machineId = machineId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordMD5() {
        return passwordMD5;
    }

    public void setPasswordMD5(String passwordMD5) {
        this.passwordMD5 = passwordMD5;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public long getClientCode() {
        return clientCode;
    }

    public void setClientCode(long clientCode) {
        this.clientCode = clientCode;
    }

    public boolean isMobile() {
        return isMobile;
    }

    public void setMobile(boolean isMobile) {
        this.isMobile = isMobile;
    }

}
