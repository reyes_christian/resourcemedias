package br.com.tibox.entity;

import com.google.gson.annotations.SerializedName;

import br.com.tibox.entity.DomainObject;

public class Resource implements DomainObject {

    private static final long serialVersionUID = 1366804971798110366L;
    
    public static final String RESOURCE_KEY = "RESOURCE";

    @SerializedName("resource_id")
    private long resourceId;

    @SerializedName("filename")
    private String fileName;

    @SerializedName("directory")
    private String directory;

    @SerializedName("url")
    private String url;

    @SerializedName("md5")
    private String md5;


	public long getResourceId() {
        return resourceId;
    }

    public void setResourceId(long resourceId) {
        this.resourceId = resourceId;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getMd5() {
        return md5;
    }

    public void setMd5(String md5) {
        this.md5 = md5;
    }

    public String getDirectory() {
        return directory;
    }

    public void setDirectory(String directory) {
        this.directory = directory;
    }

    public static String getObjectKey(long cdCliente) {
        return cdCliente + "recursos:";
    }
    
    /*public static String generateNextId() {
        return String.valueOf(resourceId++);
    }*/

	@Override
	public String toString() {
		return "Resource [resourceId=" + resourceId + ", fileName=" + fileName + ", directory=" + directory + ", url="
				+ url + ", md5=" + md5 + "]";
	}

	public String getId() {
		return String.valueOf(resourceId);
	}

	@Override
    public String getKey() {
        return getId();
    }

    @Override
    public String getObjectKey() {
        return RESOURCE_KEY;
    }

}
