package br.com.tibox.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.SerializedName;

public class MessageBulletin implements Serializable {
	
	private static final long serialVersionUID = 7262254152082852247L;

	@SerializedName("message_type")
	private String messageType;
	
	private List<Bulletin> entries;
	
	public MessageBulletin() {
	}
	
	public MessageBulletin(String messageType) {
		this.messageType = messageType;
		entries = new ArrayList<Bulletin>();
	}
	
	public String getMessageType() {
		return messageType;
	}
	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}
	public List<Bulletin> getEntries() {
		return entries;
	}
	public void setEntries(List<Bulletin> entries) {
		this.entries = entries;
	}
	
	public static String getNovosAlertasKey(long cdCliente) {
		return cdCliente + "novosalertas:";
	}
	
	public static String getAlertasKey(long cdCliente) {
		return cdCliente + "alertas:";
	}

}
