package br.com.tibox.entity;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;

public class Alert implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8884096783189150971L;

	@SerializedName("alertaId")
	private long alertaId;
	
	@SerializedName("titulo")
	private String titulo;
	
	@SerializedName("timeStamp")
	private String timeStamp;
	
	
	
	 public long getAlertaId() {
		return alertaId;
	}

	public void setAlertaId(long alertaId) {
		this.alertaId = alertaId;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getTimestamp() {
	        if (Long.valueOf(timeStamp) < 99999999999l) {
	            return Long.toString(Long.valueOf(timeStamp) * 1000);
	        }
	        return timeStamp;
	    }
	 
	 public void setTimestamp(String timeStamp) {
	        this.timeStamp = timeStamp;
	    }
}
