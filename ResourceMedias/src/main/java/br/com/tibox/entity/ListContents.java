package br.com.tibox.entity;

public class ListContents {

	private Error error;

    private MessageHistory history;

    private MessageResource resources;

    private MessageBulletin bulletins;

    private MessageDisplay newBulletins;

    private int totalRegistros;

    public Error getError() {
        return error;
    }

    public void setError(Error error) {
        this.error = error;
    }

    public MessageHistory getHistory() {
        return history;
    }

    public void setHistory(MessageHistory history) {
        this.history = history;
    }

    public MessageResource getResources() {
        return resources;
    }

    public void setResources(MessageResource resources) {
        this.resources = resources;
    }

    public MessageBulletin getBulletins() {
        return bulletins;
    }

    public void setBulletins(MessageBulletin bulletins) {
        this.bulletins = bulletins;
    }

    public MessageDisplay getNewBulletins() {
        return newBulletins;
    }

    public void setNewBulletins(MessageDisplay newBulletins) {
        this.newBulletins = newBulletins;
    }

    public int getTotalRegistros() {
        return totalRegistros;
    }

    public void setTotalRegistros(int totalRegistros) {
        this.totalRegistros = totalRegistros;
    }
}
