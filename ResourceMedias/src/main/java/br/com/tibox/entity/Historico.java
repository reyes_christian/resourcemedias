package br.com.tibox.entity;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;

import br.com.tibox.entity.DomainObject;

@SuppressWarnings("rawtypes")
public class Historico implements Serializable, DomainObject ,Comparable {

    private static final long serialVersionUID = -8950176997149825777L;
    
    public static final String HISTORICO_KEY = "HISTORICO";

    private static long id;
    
    @SerializedName("bulletin_ids")
    private long bulletinIds;
    
    @SerializedName("bulletin_time_ids")
    private long bulletinTimeIds;
    
    @SerializedName("alertaId")
    private long alertaId;
    
    private long timeStamp;

    public Historico(long alertaId, long timeStamp) {
        this.alertaId = alertaId;
        this.timeStamp = timeStamp;
    }

    public Historico() {
		// default
	}

	public long getAlertaId() {
        return alertaId;
    }

    public void setAlertaId(long alertaId) {
        this.alertaId = alertaId;
    }

    public long getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(long timeStamp) {
        this.timeStamp = timeStamp;
    }

    @Override
    public int compareTo(Object o) {
        Historico novo = (Historico) o;
        if (this.getTimeStamp() < novo.getTimeStamp()) {
            return 1;
        }
        if (this.getTimeStamp() > novo.getTimeStamp()) {
            return -1;
        }
        return 0;
    }

	@Override
	public String toString() {
		return "Historico [alertaId=" + alertaId + ", timeStamp=" + timeStamp + "]";
	}

	public String getId() {
		return String.valueOf(id);
	}

	@Override
    public String getKey() {
        return getId();
    }

    @Override
    public String getObjectKey() {
        return HISTORICO_KEY;
    }
}
