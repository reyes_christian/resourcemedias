package br.com.tibox.entity;

public class Image implements DomainObject{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1766601018827457899L;

	public static final String OBJECT_KEY = "IMAGE";

	 public Image() {
	 }

	 public Image(String imageId, String imageAsStringBase64){
	  this.imageId = imageId;
	  this.imageAsStringBase64 = imageAsStringBase64;
	 }
	 private String imageId;
	 private String imageAsStringBase64;

	 public String getImageId() {
	  return imageId;
	 }

	 public void setImageId(String imageId) {
	  this.imageId = imageId;
	 }

	 public String getImageName() {
	  return imageAsStringBase64;
	 }

	 public void setImageName(String imageAsStringBase64) {
	  this.imageAsStringBase64 = imageAsStringBase64;
	 }

	 @Override
	 public String toString() {
	  return "User [id=" + imageAsStringBase64 + ", imageAsBase64String=" + imageAsStringBase64 + "]";
	 }

	 @Override
	 public String getKey() {
	  return getImageId();
	 }

	 @Override
	 public String getObjectKey() {
	  return OBJECT_KEY;
	 }
}
