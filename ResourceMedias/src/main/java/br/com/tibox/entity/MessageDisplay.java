package br.com.tibox.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.SerializedName;

public class MessageDisplay implements Serializable {

    private static final long serialVersionUID = 7362254158082852247L;

    @SerializedName("message_type")
    private String messageType;

    private List<Display> entries;

    public MessageDisplay() {
    }

    public MessageDisplay(String messageType) {
        this.messageType = messageType;
        entries = new ArrayList<Display>();
    }

    public String getMessageType() {
        return messageType;
    }

    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }

    public List<Display> getEntries() {
        return entries;
    }

    public void setEntries(List<Display> entries) {
        this.entries = entries;
    }

    public static String getNovosAlertasKey(long cdCliente) {
        return cdCliente + "novosalertas:";
    }

    public static String getAlertasKey(long cdCliente) {
        return cdCliente + "alertas:";
    }
}
