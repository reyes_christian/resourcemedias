package br.com.tibox.services;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.stereotype.Service;

import br.com.tibox.entity.Bulletin;

@Service("refreshHistoryItemService")
public class RefreshHistoryItemService implements Runnable{
	
	protected Thread thread = null;

    private boolean active = false;
    private static final Logger logger = Logger.getLogger(RefreshHistoryItemService.class.getName());

    @Override
    public void run() {
        try {
            logger.info("Iniciando thread que avalia o status dos itens do historico");
            while (this.active) {
                List<Bulletin> listaTmp = HistoricoService.getListaBulletins();
                if (listaTmp != null) {
                    for (int i = 0; i < listaTmp.size(); i++) {
                        HistoricoService.getListaBulletins().set(i, listaTmp.get(i));
                    }
                }
            }
            Thread.sleep(10000l);
        }catch (Exception e) {
            logger.log(Level.SEVERE, "Erro ao avaliar o status dos itens do historico", e.getMessage());
        }
    }
 

    public void start() {
        if (!this.active) {
            this.active = true;
            this.thread = new Thread(this, "RefreshHistoryItem");
            this.thread.start();
        }
    }

    public void stop() {
        if (this.active) {
            this.active = false;
            this.thread.notifyAll();
            while (this.thread.getState() != Thread.State.TERMINATED) {
                try {
                    Thread.sleep(400L);
                } catch (InterruptedException e) {
                    logger.severe(e.getMessage());
                }
            }
            logger.info("Parando thread que avalia o status dos itens do historico");
        }
    }

}
