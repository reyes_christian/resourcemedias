package br.com.tibox.services;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.stereotype.Service;

import br.com.tibox.entity.MessageResource;
import br.com.tibox.entity.Resource;
import br.com.tibox.utils.FileUtils;

@Service("downloadResourcesService")
public class DownloadResourcesService implements Runnable{

	protected Thread thread = null;

    private boolean active = false;

    private static boolean downloadInProgress = false;

    private static final Logger logger = Logger.getLogger(DownloadResourcesService.class.getName());

    @Override
    public void run() {
        try {
            logger.info("Iniciando thread de download de resources");
            while (this.active) {
                if (HistoricoService.getContents() != null) {
                    MessageResource resources = HistoricoService.getContents().getResources();
                    for (Resource resource : resources.getEntries()) {
                        if (!FileUtils.resourceIsComplete(resource.getFileName(), resource.getMd5())) {
                            FileUtils.downloadFile(resource.getUrl(), resource.getFileName(), resource.getMd5(), downloadInProgress);
                        }
                    }
                }
                Thread.sleep(10000l);
            }
        } catch (Exception e) {
            logger.log(Level.SEVERE, "Erro", e);
        }
    }

    public void start() {
        if (!this.active) {
            this.active = true;
            this.thread = new Thread(this, "DownloadResources");
            this.thread.start();
        }
    }

    public void stop() {
        if (this.active) {
            this.active = false;
            this.thread.notifyAll();
            while (this.thread.getState() != Thread.State.TERMINATED) {
                try {
                    Thread.sleep(400L);
                } catch (InterruptedException e) {
                    logger.severe(e.getMessage());
                }
            }
            logger.info("Parando thread de download de resources");
        }
    }

}
