package br.com.tibox.services;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.naming.AuthenticationException;
import javax.ws.rs.core.MediaType;
import javax.xml.ws.WebServiceException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientHandlerException;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.UniformInterfaceException;
import com.sun.jersey.api.client.WebResource;

import br.com.tibox.entity.Bulletin;
import br.com.tibox.entity.ClienteRequest;
import br.com.tibox.entity.Historico;
import br.com.tibox.entity.ListContents;
import br.com.tibox.utils.ClientHelper;
import br.com.tibox.utils.Constants;

@Service("historicoService")
public class HistoricoService {
	
	private static ListContents contents;
	
	private static List<Bulletin> listaBulletins;
	
	@Autowired
	private DownloadResourcesService downloadResources;
	
	@Autowired
	private RefreshHistoryItemService refreshHistoryItem;
	
	@Autowired
	private RefreshHistoryService refreshHistory;
	
	private String machineId;
	
	private String username;
	
	
	public String getHistoricoInfo(String machineId, String username){
		
		 try {
        	this.machineId = machineId;
        	this.username = username;
        	ClienteRequest cr = new ClienteRequest();
            cr.setClientCode(Constants.CLIENT_CODE);
            cr.setUserId(this.username);
            cr.setMachineId(this.machineId);
            cr.setMobile(false);
            cr.setTotalRegistros(Constants.getHistoricoTamanho());

            ListContents conteudo = sendPost(Constants.URL_LISTA_HISTORICO, new Gson().toJson(cr));
            String teste = new Gson().toJson(conteudo, ListContents.class);
            
            return teste;
		 }catch (Exception ex) {
            ex.printStackTrace();
            Logger.getLogger(HistoricoService.class.getName()).log(Level.SEVERE, "Falha ao listar o historico", ex);
     		}
		
		
		return "";
	}
	
	
	public void getHistorico(String machineId, String username) {
        try {
          
        	this.machineId = machineId;
        	this.username = username;
        	ClienteRequest cr = new ClienteRequest();
            cr.setClientCode(Constants.CLIENT_CODE);
            cr.setUserId(username);
            cr.setMachineId(machineId);
            cr.setMobile(false);
            cr.setTotalRegistros(Constants.getHistoricoTamanho());

            this.setContents(this.sendPost(Constants.URL_LISTA_HISTORICO, new Gson().toJson(cr)));
            

            downloadResources.start();
            refreshHistoryItem.start();
            refreshHistory.start(this);
            //showNewAllert.start();

        } catch (Exception ex) {
            ex.printStackTrace();
            Logger.getLogger(HistoricoService.class.getName()).log(Level.SEVERE, "Falha ao listar o historico", ex);
        }
    }

	 public ListContents sendPost(String url, String dados) throws Exception {

		 Client client = ClientHelper.createClient();
	        try {
	          
	            WebResource webResource = client.resource(url);
	            ClientResponse response = webResource.type(MediaType.APPLICATION_JSON).post(ClientResponse.class, dados);

	            if (response.getStatus() != 200) {
	                if (response.getStatus() == 401) {
	                    throw new AuthenticationException();
	                }
	                throw new WebServiceException();
	            }

	            String retorno = response.getEntity(String.class);
	            ListContents conteudos = new Gson().fromJson(retorno, ListContents.class);
	            return conteudos;

	        } catch (UniformInterfaceException | AuthenticationException | WebServiceException | ClientHandlerException | JsonSyntaxException e) {
	        	 Logger.getLogger(HistoricoService.class.getName()).log(Level.SEVERE, "Falha ao listar o historico", e);
	            throw e;
	        } finally {
	            if (client != null) {
	                client.destroy();
	                client = null;
	            }
	        }
	    }
	 
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public synchronized void setContents(ListContents aContents) {
	        contents = aContents;

	        listaBulletins = new ArrayList();
	        Collections.sort(getContents().getHistory().getBulletinTimeIds());
	        for (Historico historico : getContents().getHistory().getBulletinTimeIds()) {
	            for (Bulletin bulletin : getContents().getBulletins().getEntries()) {
	                if (bulletin.getBulletinId() == historico.getAlertaId()) {
	                    getListaBulletins().add(bulletin);
	                }
	            }
	        }

	        preencherHistorico(getListaBulletins());

	    }
	
	public synchronized static ListContents getContents() {
        return contents;
    }
	
	 public synchronized static List<Bulletin> getListaBulletins() {
	        return listaBulletins;
	    }
	
    public void preencherHistorico(List<Bulletin> data) {
    	
    }

	public String getMachineId() {
		return machineId;
	}

	public void setMachineId(String machineId) {
		this.machineId = machineId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public DownloadResourcesService getDownloadResources() {
		return downloadResources;
	}

	public void setDownloadResources(DownloadResourcesService downloadResources) {
		this.downloadResources = downloadResources;
	}


	public RefreshHistoryItemService getRefreshHistoryItem() {
		return refreshHistoryItem;
	}


	public void setRefreshHistoryItem(RefreshHistoryItemService refreshHistoryItem) {
		this.refreshHistoryItem = refreshHistoryItem;
	}


	public RefreshHistoryService getRefreshHistory() {
		return refreshHistory;
	}


	public void setRefreshHistory(RefreshHistoryService refreshHistory) {
		this.refreshHistory = refreshHistory;
	}
    
    /*public boolean temFiltroPreenchido() {
        if ((this.titulo != null && !this.titulo.getText().isEmpty())
                || (this.data != null && this.data.getValue() != null)) {
            return true;
        }
        return false;
    }*/
}
