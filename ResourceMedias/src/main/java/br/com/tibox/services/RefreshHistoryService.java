package br.com.tibox.services;

import com.google.gson.Gson;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.stereotype.Service;

import br.com.tibox.entity.ClienteRequest;
import br.com.tibox.utils.Constants;

@Service("refreshHistoryService")
public class RefreshHistoryService implements Runnable{
	
	protected Thread thread = null;

    private HistoricoService historicoService;

    private boolean active = false;
    
    private static final Logger logger = Logger.getLogger(RefreshHistoryService.class.getName());

    @Override
    public void run() {
        try {
            Thread.sleep(Constants.getTempoPing());
            logger.info("Iniciando thread RefreshHistoryService que atualiza o historico!");
            while (this.active) {
                try {
                        ClienteRequest cr = new ClienteRequest();
                        cr.setClientCode(Constants.CLIENT_CODE);
                        cr.setUserId(historicoService.getUsername());
                        cr.setMachineId(historicoService.getMachineId());
                        cr.setMobile(false);
                        cr.setTotalRegistros(Constants.getHistoricoTamanho());
                        historicoService.setContents(historicoService.sendPost(Constants.URL_LISTA_HISTORICO, new Gson().toJson(cr)));
                } catch (Exception ex) {
                    logger.severe(ex.getMessage());
                }
           };

                Thread.sleep(Constants.getTempoPing());
        } catch (Exception e) {
            logger.log(Level.SEVERE, "Erro", e);
        }
    }

    public void start(HistoricoService historicoService) {
        if (!this.active) {
            this.historicoService = historicoService;
            this.active = true;
            this.thread = new Thread(this, "RefreshHistory");
            this.thread.start();
        }
    }

    public void stop() {
        if (this.active) {
            this.active = false;
            this.thread.notifyAll();
            while (this.thread.getState() != Thread.State.TERMINATED) {
                try {
                    Thread.sleep(400L);
                } catch (InterruptedException e) {
                    logger.severe(e.getMessage());
                }
            }
            logger.info("Parando thread que atualiza o historico!");
        }
    }

}
