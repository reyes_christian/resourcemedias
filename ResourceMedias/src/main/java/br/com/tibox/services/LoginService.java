package br.com.tibox.services;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.naming.AuthenticationException;
import javax.ws.rs.core.MediaType;
import javax.xml.ws.WebServiceException;

import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.UniformInterfaceException;
import com.sun.jersey.api.client.WebResource;

import br.com.tibox.entity.ClientLogin;
import br.com.tibox.entity.LoginResponse;
import br.com.tibox.utils.ClientHelper;
import br.com.tibox.utils.Constants;

@Service("loginService")
public class LoginService {

	
	public LoginResponse logar(String usuario, String senha, String machineId) {

        try {
        	Logger.getLogger(LoginService.class.getName()).log(Level.INFO, "Realizando Login");
        	
            ClientLogin cl = new ClientLogin();
            cl.setClientCode(Constants.CLIENT_CODE);
            cl.setUsername(usuario);
            cl.setPassword(senha);
            cl.setMobile(false);
            cl.setMachineId(machineId);

            ClientResponse response;
			try {
				response = this.sendPost(Constants.URL_LOGIN, new Gson().toJson(cl));
				String retorno = response.getEntity(String.class);
				LoginResponse aut = new Gson().fromJson(retorno, LoginResponse.class);
				 if (aut.isAuthenticated()) {
		            	return aut;
		            }
			} catch (Exception e) {
				Logger.getLogger(LoginService.class.getName()).log(Level.SEVERE, "Problema ao fazer Login", e);
				e.printStackTrace();
			}
        	}catch (Exception e) {
        		Logger.getLogger(LoginService.class.getName()).log(Level.SEVERE, "Problema ao fazer Login", e);
				e.printStackTrace();
        }
        return new LoginResponse();
	}
	
	 private ClientResponse sendPost(String url, String dados) throws Exception {
	
	        Client client = ClientHelper.createClient();
	        try {
	            
	            WebResource webResource = client.resource(url);
	            ClientResponse response = webResource.type(MediaType.APPLICATION_JSON).post(ClientResponse.class, dados);
	
	            if (response.getStatus() != 200) {
	                if (response.getStatus() == 401) {
	                    throw new AuthenticationException();
	                }
	                throw new WebServiceException();
	            }
	
	            return response;
	        } catch (UniformInterfaceException | AuthenticationException | WebServiceException e) {
	            throw e;
	        } finally {
	            if (client != null) {
	                client.destroy();
	                client = null;
	            }
	        }
	    }
}
