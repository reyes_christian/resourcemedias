package br.com.tibox.services;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.naming.AuthenticationException;
import javax.xml.ws.WebServiceException;

import org.springframework.stereotype.Service;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.UniformInterfaceException;
import com.sun.jersey.api.client.WebResource;

import br.com.tibox.utils.ClientHelper;

@Service("contratoService")
public class ContratoService {
	

    public JsonObject sendPostContrato(String url, String dados) throws Exception {
    	
    	Logger.getLogger(ContratoService.class.getName()).log(Level.INFO, "Realizando busca do contrato");
    	JsonObject contrato = new JsonObject();
    	Client client = ClientHelper.createClient();
        try {
        	
            WebResource webResource = client.resource(url);
            ClientResponse response = webResource.type(javax.ws.rs.core.MediaType.APPLICATION_JSON).post(ClientResponse.class, dados);

            if (response.getStatus() != 200) {
                if (response.getStatus() == 401) {
                    throw new AuthenticationException();
                }
                throw new WebServiceException();
            }

            String retorno = response.getEntity(String.class);
            System.out.println(retorno);
            JsonParser parser = new JsonParser();
            contrato = parser.parse(retorno).getAsJsonObject();
            
            /*{"messageType":"C","configuration":{"client_id":101,"velocidade_descarga":10,"tempo_requisicao":1,
            	"qtd_historico":10,"tipo_autenticacao":2,"tempo_requisicao_mobile":0,"botao_sair":0},"newAlert":true}
            
             
			 contrato.setClientId(obj.get("client_id").getAsLong());
			 contrato.setVelocidadeDescarga(obj.get("velocidade_descarga").getAsInt());
			 contrato.setTempoRequisicao(obj.get("tempo_requisicao").getAsInt());
			 contrato.setQtdHistorico(obj.get("qtd_historico").getAsInt());
			 contrato.setTipoAutenticacao(obj.get("tipo_autenticacao").getAsInt()); */
			 
			 
            return contrato;

        } catch (UniformInterfaceException | AuthenticationException | WebServiceException e) {
        	Logger.getLogger(ContratoService.class.getName()).log(Level.SEVERE, "Erro busca do contrato");

            throw e;
        } finally {
            if (client != null) {
                client.destroy();
                client = null;
            }
        }
    }
}
