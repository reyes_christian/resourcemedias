package br.com.tibox.jobs;

import br.com.tibox.utils.FileUtils;

public class CheckUpdateJob {
	
	private String clienteCode;
	
	private String machineId;
	
	private String userId;
	
	private String password;

	private FileUtils utils;
	
	private Boolean mobile;
	
	private Integer totalRegistro;
	
	private Integer pagina;

	public String getClienteCode() {
		return clienteCode;
	}

	public void setClienteCode(String clienteCode) {
		this.clienteCode = clienteCode;
	}

	public String getMachineId() {
		return machineId;
	}

	public void setMachineId(String machineId) {
		this.machineId = machineId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public FileUtils getUtils() {
		return utils;
	}

	public void setUtils(FileUtils utils) {
		this.utils = utils;
	}

	public Boolean getMobile() {
		return mobile;
	}

	public void setMobile(Boolean mobile) {
		this.mobile = mobile;
	}

	public Integer getTotalRegistro() {
		return totalRegistro;
	}

	public void setTotalRegistro(Integer totalRegistro) {
		this.totalRegistro = totalRegistro;
	}

	public Integer getPagina() {
		return pagina;
	}

	public void setPagina(Integer pagina) {
		this.pagina = pagina;
	}
}
