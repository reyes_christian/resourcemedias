package br.com.tibox.utils;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.imageio.ImageIO;

import com.google.common.hash.HashCode;
import com.google.common.hash.Hashing;
import com.google.common.io.Files;

import br.com.tibox.entity.Resource;
import redis.clients.util.RedisInputStream;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

@SuppressWarnings("restriction")
public class FileUtils {
	
	private static final Logger logger = Logger.getLogger(FileUtils.class.getName());

    public static int getFileSize(URL url) {
        HttpURLConnection conn = null;
        try {
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("HEAD");
            conn.getInputStream();
            return conn.getContentLength();
        } catch (IOException e) {
            return -1;
        } finally {
            if (conn != null) {
                conn.disconnect();
                conn = null;
            }
        }
    }
    
    
    public RedisInputStream downloadSaveFile(Resource resource){
    	 URL urlArquivo;
		try {
			urlArquivo = new URL(resource.getUrl());
			RedisInputStream in=new RedisInputStream(urlArquivo.openStream());
			return in;
		}catch (IOException e) {
			e.printStackTrace();
		}
    	return null;
    	
    }

    public static void downloadFile(String endereco, String nomeArquivo, String MD5, boolean downloadInProgress) throws Exception {

        ReadableByteChannel rbc = null;
        FileOutputStream fos = null;
        try {
            URL urlArquivo = new URL(endereco);
            rbc = Channels.newChannel(urlArquivo.openStream());

            File diretorio = new File(Constants.HOME_BULLETIN);
            if (!diretorio.exists()) {
                diretorio.mkdirs();
            }

            File arquivo = new File(Constants.HOME_BULLETIN + nomeArquivo);
            arquivo.createNewFile();

            fos = new FileOutputStream(arquivo.getAbsolutePath());

            long tamanhoArquivo = FileUtils.getFileSize(urlArquivo);
            long totalBaixado = 0;
            logger.log(Level.SEVERE, "Baixando o arquivo: {0}", nomeArquivo);
            while (totalBaixado <= tamanhoArquivo) {
                fos.getChannel().transferFrom(rbc, totalBaixado, Constants.HISTORICO_TAMANHO);
                totalBaixado += Constants.getDownloadLimit();
                fos.flush();
                Thread.sleep(1000l);
            }
            if (FileUtils.resourceIsComplete(nomeArquivo, MD5)) {
                downloadInProgress = false;
            } else {
                logger.log(Level.SEVERE, "Erro ao checar o MD5 do arquivo {0}", nomeArquivo);
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (rbc != null) {
                rbc.close();
                rbc = null;
            }
            if (fos != null) {
                fos.close();
                fos = null;
            }
        }
    }

    public static boolean resourceIsComplete(String nomeArquivo, String MD5) throws Exception {
        File arquivo = new File(Constants.HOME_BULLETIN + File.separator + nomeArquivo);
        if (arquivo.exists()) {
            HashCode md5Local = Files.hash(arquivo, Hashing.md5());
            if (MD5 == null || md5Local.toString().equals(MD5)) {
                return true;
            }
        }
        return false;
    }

	public static void copyContentToHomeUser(Class<?> classe) throws Exception {

        InputStream in = null;
        OutputStream out = null;

        try {
            
            createNecessaryFolder();
            
            in = classe.getResourceAsStream("/images/red-close.png");
            out = new FileOutputStream(Constants.HOME_BULLETIN + "red-close.png");
            copyStream(in, out);
            
            in = classe.getResourceAsStream("/images/icone-allert.png");
            out = new FileOutputStream(Constants.HOME_BASIC + "images" + File.separator + "icone-allert.png");
            copyStream(in, out);
            
            in = classe.getResourceAsStream("/images/icone-allert-pequeno.png");
            out = new FileOutputStream(Constants.HOME_BASIC + "images" + File.separator + "icone-allert-pequeno.png");
            copyStream(in, out);
            
            in = classe.getResourceAsStream("/html/login.html");
            out = new FileOutputStream(Constants.HOME_BASIC + "html" + File.separator + "login.html");
            copyStream(in, out);
            
            in = classe.getResourceAsStream("/html/background-login.jpg");
            out = new FileOutputStream(Constants.HOME_BASIC + "html" + File.separator + "background-login.jpg");
            copyStream(in, out);
            
            in = classe.getResourceAsStream("/styles/historico.css");
            out = new FileOutputStream(Constants.HOME_BASIC + "styles" + File.separator + "historico.css");
            copyStream(in, out);
            
            in = classe.getResourceAsStream("/styles/login.css");
            out = new FileOutputStream(Constants.HOME_BASIC + "styles" + File.separator + "login.css");
            copyStream(in, out);
            
            in = classe.getResourceAsStream("/styles/bootstrap.min-3.3.5.css");
            out = new FileOutputStream(Constants.HOME_BASIC + "styles" + File.separator + "bootstrap.min-3.3.5.css");
            copyStream(in, out);

        } catch (Exception e) {
            throw e;
        } finally {
            if (in != null) {
                in.close();
            }
            if (out != null) {
                out.close();
            }
        }
    }


    private static void createNecessaryFolder() throws IOException {
        File BulletinDir = new File(Constants.HOME_BULLETIN);
        if (!BulletinDir.exists()) {
            BulletinDir.mkdirs();
        }

        File BasicDir = new File(Constants.HOME_BASIC + "html");
        if (!BasicDir.exists()) {
            BasicDir.mkdirs();
        }
        
        BasicDir = new File(Constants.HOME_BASIC + "styles");
        if (!BasicDir.exists()) {
            BasicDir.mkdirs();
        }
        
        BasicDir = new File(Constants.HOME_BASIC + "images");
        if (!BasicDir.exists()) {
            BasicDir.mkdirs();
        }
    }
    
    private static void copyStream(InputStream in, OutputStream out) throws IOException {
        byte[] buffer = new byte[1024];
        int lenght;
        while ((lenght = in.read(buffer)) > 0) {
            out.write(buffer, 0, lenght);
        }
    }
    
    /**
     * Decode string to image
     * @param imageString The string to decode
     * @return decoded image
     */
	public static BufferedImage decodeToImage(String imageString) {

        BufferedImage image = null;
        byte[] imageByte;
        try {
			BASE64Decoder decoder = new BASE64Decoder();
            imageByte = decoder.decodeBuffer(imageString);
            ByteArrayInputStream bis = new ByteArrayInputStream(imageByte);
            image = ImageIO.read(bis);
            bis.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return image;
    }

    /**
     * Encode image to string
     * @param image The image to encode
     * @param type jpeg, bmp, ...
     * @return encoded string
     */
	public static String encodeToString(BufferedImage image, String type) {
        String imageString = null;
        ByteArrayOutputStream bos = new ByteArrayOutputStream();

        try {
            ImageIO.write(image, type, bos);
            byte[] imageBytes = bos.toByteArray();

            BASE64Encoder encoder = new BASE64Encoder();
            imageString = encoder.encode(imageBytes);

            bos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return imageString;
    }
	
}
