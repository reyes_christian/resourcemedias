package br.com.tibox.utils;

import java.io.File;

public class Constants {
	
    public static final String URL_LOGIN = "https://tbxsp.allert.com.br/WS/rest/accessible/autenticar";
    public static final String URL_LISTA_HISTORICO = "https://tbxsp.allert.com.br/WS/rest/restricted/listar";
    public static final String URL_EVENTO = "https://tbxsp.allert.com.br/WS/rest/restricted/evento";
    public static final String URL_CONTRATO = "https://tbxsp.allert.com.br/WS/rest/restricted/contrato";
    public static final String URL_RSC = "https://tbxsp.allert.com.br/WS/rsc/";
    public static final long CLIENT_CODE = 101;
    public static final String USER_AGENT = "JAVA";
    public static final int HISTORICO_TAMANHO = 10;
    public static final String HOME_USER = System.getProperty("user.home") + File.separator + "Player" + File.separator;
    public static final String HOME_BULLETIN = HOME_USER + File.separator + "Bulletin" + File.separator;
    public static final String HOME_BASIC = HOME_USER + "Basic" + File.separator;
    
	

    private static int downloadLimit = 25120;
    //Tempo em milissegundos
    private static long tempoPing = 10000;

    public static int getDownloadLimit() {
        return downloadLimit;
    }

    /**
     * E necessario multiplicar o valor pois vem em kbps e utilizamos em bytes o valor.
     * @param aDownloadLimit 
     */
    public static void setDownloadLimit(int aDownloadLimit) {
        downloadLimit = aDownloadLimit * 1024;
    }

    public static int getHistoricoTamanho() {
        return HISTORICO_TAMANHO;
    }


    public static long getTempoPing() {
        return tempoPing;
    }

    /**
     * E necessario mutiplicar pois recebemos o tempo em minutos e precisamos dele em milisegundos
     * @param aTempoPing 
     */
    public static void setTempoPing(long aTempoPing) {
        tempoPing = (aTempoPing * 60) * 1000;
    }


}


