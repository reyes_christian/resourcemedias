package br.com.tibox.controllers;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import br.com.tibox.entity.Historico;
import br.com.tibox.entity.ListContents;
import br.com.tibox.services.HistoricoService;

@RestController
public class HistoricoController {

	@Autowired
	private HistoricoService historicoService;
	
	private String machineId;
	
	private String userName;
	
	private ListContents contents;
	
	private Historico historico;
	
	
	@RequestMapping(value = "/historico", method = RequestMethod.POST)
	@ResponseBody
	public void getHistorico(@RequestBody String body){
		
		if(body != null){
			Logger.getLogger(HistoricoController.class.getName()).log(Level.INFO, "Teste");
			JsonParser parser = new JsonParser();
			JsonObject obj = parser.parse(body).getAsJsonObject();
			
			this.userName = obj.get("user_id").getAsString();
			this.machineId = obj.get("machine_id").getAsString();
			Logger.getLogger(HistoricoController.class.getName()).log(Level.INFO, "Realizando chamada ao servico Historico");
			
			try {
				 historicoService.getHistorico(machineId, userName);
				
			} catch (Exception e) {
				Logger.getLogger(HistoricoController.class.getName()).log(Level.SEVERE, "ERRO Realizando chamada ao servico Contrato");
				e.printStackTrace();
			}
			
		}
	}
	
	
	@RequestMapping(value = "/info/historico", method = RequestMethod.POST)
	@ResponseBody
	public String getHistoricoInfo(@RequestBody String body){
		String resp ="";
		if(body != null){
			Logger.getLogger(HistoricoController.class.getName()).log(Level.INFO, "Teste");
			JsonParser parser = new JsonParser();
			JsonObject obj = parser.parse(body).getAsJsonObject();
			
			this.userName = obj.get("user_id").getAsString();
			this.machineId = obj.get("machine_id").getAsString();
			Logger.getLogger(HistoricoController.class.getName()).log(Level.INFO, "Realizando chamada ao servico de informacao de historicos");
			
			try {
				return resp = historicoService.getHistoricoInfo(machineId, userName);
				
			} catch (Exception e) {
				Logger.getLogger(HistoricoController.class.getName()).log(Level.SEVERE, "ERRO Realizando chamada ao servico Contrato");
				e.printStackTrace();
			}
			
		}
		return resp;
	}

	public HistoricoService getHistoricoService() {
		return historicoService;
	}

	public void setHistoricoService(HistoricoService historicoService) {
		this.historicoService = historicoService;
	}

	public String getMachineId() {
		return machineId;
	}

	public void setMachineId(String machineId) {
		this.machineId = machineId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public ListContents getContents() {
		return contents;
	}

	public void setContents(ListContents contents) {
		this.contents = contents;
	}

	public Historico getHistorico() {
		return historico;
	}

	public void setHistorico(Historico historico) {
		this.historico = historico;
	}
}
