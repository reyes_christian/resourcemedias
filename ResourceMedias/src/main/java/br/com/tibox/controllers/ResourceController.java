package br.com.tibox.controllers;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.servlet.http.HttpServletResponse;

import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.tibox.utils.Constants;

@RestController
public class ResourceController {
	
	
    @RequestMapping(value = "/{clientID}/{bulletinID}", method = RequestMethod.GET)
    public void getResources(@PathVariable("clientID") String clientID,
    					@PathVariable("bulletinID") String bulletinID, 
    					HttpServletResponse response) throws IOException {
            String src= Constants.HOME_BULLETIN;
            InputStream is = new FileInputStream(src);
            IOUtils.copy(is, response.getOutputStream());
            response.flushBuffer();
    }

}
