package br.com.tibox.controllers;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import br.com.tibox.services.ContratoService;
import br.com.tibox.utils.Constants;

@RestController
public class ContratoController {
	
	@Autowired
	private ContratoService contratoService;
	
	private boolean newAlert;
	
	/**
	 * 
	 */
	@RequestMapping(value = "/contrato", method = RequestMethod.POST)
	@ResponseBody
	public String getContrato(@RequestBody String body){
		
		JsonObject contrato = new JsonObject();
		
		if(body != null){
			Logger.getLogger(ContratoController.class.getName()).log(Level.INFO, "Teste");
			JsonParser parser = new JsonParser();
			JsonObject obj = parser.parse(body).getAsJsonObject();
			
			Logger.getLogger(LoginController.class.getName()).log(Level.INFO, "Realizando chamada ao servico Contrato");
			
			try {
				contrato = contratoService.sendPostContrato(Constants.URL_CONTRATO, new Gson().toJson(obj));
				
				this.newAlert = contrato.get("newAlert").getAsBoolean();
			} catch (Exception e) {
				Logger.getLogger(LoginController.class.getName()).log(Level.SEVERE, "ERRO Realizando chamada ao servico Contrato");
				e.printStackTrace();
			}
			
			if(this.newAlert == true  ){
				Logger.getLogger(LoginController.class.getName()).log(Level.INFO, "Há novo alerta");
				return contrato.toString();
			}
			else{
				Logger.getLogger(LoginController.class.getName()).log(Level.INFO, "Nao Há novo alerta");
			}
			
			return contrato.toString();
		}
		
	return contrato.toString();	
		
	}
	
	
	public ContratoService getContratoService() {
		return contratoService;
	}

	public void setContratoService(ContratoService contratoService) {
		this.contratoService = contratoService;
	}


}
