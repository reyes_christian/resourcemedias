package br.com.tibox.controllers;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import br.com.tibox.entity.LoginResponse;
import br.com.tibox.services.LoginService;

@RestController
public class LoginController {
	
	@Autowired
	private LoginService loginService;
	
	private String userName;
	private String password;
	private String machineId;
	private String clientCode;
	
	/**
	 * 
	 */
	@RequestMapping(value = "/autenticar", method = RequestMethod.POST)
	@ResponseBody
	public LoginResponse login(@RequestBody String body){
		
		/*this.userName = "11cdf86d5723eecce5af1f33e5fde9f066e608d0a1068f445d99820eef5c19ae";
		this.password = "18138372fad4b94533cd4881f03dc6c69296dd897234e0cee83f727e2e6b1f63";
		this.machineId = "78-2B-CB-C3-8E-70";
		this.clientCode = "101";*/
		
		if(body != null){
			Logger.getLogger(LoginController.class.getName()).log(Level.INFO, "Teste");
			JsonParser parser = new JsonParser();
			 JsonObject obj = parser.parse(body).getAsJsonObject();
			 this.userName = obj.get("username").getAsString();
			 this.password = obj.get("password").getAsString();
			 this.machineId = obj.get("machine_id").getAsString();
			 this.clientCode = obj.get("client_code").getAsString();
			 
			 LoginResponse aut = new LoginResponse();
             Logger.getLogger(LoginController.class.getName()).log(Level.INFO, "Realizando chamada ao servico Login");
				
			 aut = loginService.logar(userName, password, machineId);
				
			if(aut.isAuthenticated()){
				Logger.getLogger(LoginController.class.getName()).log(Level.INFO, "Autenticado");
				return aut;
			}
			else{
				Logger.getLogger(LoginController.class.getName()).log(Level.INFO, "Nao Autenticado");
				aut.setAuthenticated(false);
			}
			return aut;
		}
		return new LoginResponse();
	}

	
	public LoginService getLoginService() {
		return loginService;
	}


	public void setLoginService(LoginService loginService) {
		this.loginService = loginService;
	}


	public String getUserName() {
		return userName;
	}


	public String getPassword() {
		return password;
	}


	public String getMachineId() {
		return machineId;
	}


	public String getClientCode() {
		return clientCode;
	}

}
