package br.com.tibox.repositories;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;

import br.com.tibox.entity.Image;

public class ImageRepository implements Repository<Image>{
	
	@Autowired
	private RedisTemplate<String,Image> redisTemplate;

	public RedisTemplate<String,Image> getRedisTemplate() {
		return redisTemplate;
	 }

	public void setRedisTemplate(RedisTemplate<String,Image> redisTemplate) {
		this.redisTemplate = redisTemplate;
	 }


	@Override
	public void put(Image obj) {
		redisTemplate.opsForHash()
	    .put(obj.getObjectKey(), obj.getKey(), obj);
	}

	@Override
	public Image get(Image key) {
		return (Image) redisTemplate.opsForHash().get(key.getObjectKey(),
			    key.getKey());
	}

	@Override
	public void delete(Image key) {
		redisTemplate.opsForHash().delete(key.getObjectKey(), key.getKey());
	}

	@Override
	public List<Image> getObjects() {
		List<Image> images = new ArrayList<Image>();
			for (Object image : redisTemplate.opsForHash().values(Image.OBJECT_KEY) ){
				images.add((Image) image);
			}
		return images;
	}

}
