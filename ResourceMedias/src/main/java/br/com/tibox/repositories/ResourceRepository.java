package br.com.tibox.repositories;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;

import br.com.tibox.entity.Resource;

public class ResourceRepository implements Repository<Resource>{
	
	@Autowired
	private RedisTemplate<String,Resource> redisTemplate;
	  
	public RedisTemplate<String,Resource> getRedisTemplate() {
		return redisTemplate;
	}
	 
	public void setRedisTemplate(RedisTemplate<String,Resource> redisTemplate) {
		this.redisTemplate = redisTemplate;
	}

	@Override
	public void put(Resource obj) {
		redisTemplate.opsForHash()
	    .put(obj.getObjectKey(), obj.getKey(), obj);
		
	}

	@Override
	public Resource get(Resource key) {
		return (Resource) redisTemplate.opsForHash().get(key.getObjectKey(),
			    key.getKey());
	}

	@Override
	public void delete(Resource key) {
		redisTemplate.opsForHash().delete(key.getObjectKey(), key.getKey());
		
	}

	@Override
	public List<Resource> getObjects() {
	List<Resource> resources = new ArrayList<Resource>();
	  for (Object resource : redisTemplate.opsForHash().values(Resource.RESOURCE_KEY) ){
		  resources.add((Resource) resource);
	  }
	  return resources;
	}

}
