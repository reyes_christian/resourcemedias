package br.com.tibox.repositories;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;

import br.com.tibox.entity.Historico;

public class HistoryRepository implements Repository<Historico>{
	
	@Autowired
	private RedisTemplate<String,Historico> redisTemplate;

	public RedisTemplate<String,Historico> getRedisTemplate() {
		return redisTemplate;
	 }

	public void setRedisTemplate(RedisTemplate<String,Historico> redisTemplate) {
		this.redisTemplate = redisTemplate;
	 }

	@Override
	public void put(Historico obj) {
		redisTemplate.opsForHash()
	    .put(obj.getObjectKey(), obj.getKey(), obj);
	}

	@Override
	public Historico get(Historico key) {
		return (Historico) redisTemplate.opsForHash().get(key.getObjectKey(),
			    key.getKey());
	}

	@Override
	public void delete(Historico key) {
		redisTemplate.opsForHash().delete(key.getObjectKey(), key.getKey());
		
	}

	@Override
	public List<Historico> getObjects() {
		List<Historico> historicos = new ArrayList<Historico>();
		for (Object historico : redisTemplate.opsForHash().values(Historico.HISTORICO_KEY) ){
			historicos.add((Historico) historico);
		}
	return historicos;
}

}
