package br.com.tibox.repositories;

import java.util.List;

import br.com.tibox.entity.DomainObject;

public interface Repository <V extends DomainObject>{
	
	void put(V obj);
	 
	V get(V key);
	
	void delete(V key);
	  
	List<V> getObjects();
	 
}
