package br.com.tibox.repositories;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;

import br.com.tibox.entity.Bulletin;

public class BulletinRepository implements Repository<Bulletin>{
	
	@Autowired
	private RedisTemplate<String,Bulletin> redisTemplate;

	public RedisTemplate<String,Bulletin> getRedisTemplate() {
		return redisTemplate;
	 }

	public void setRedisTemplate(RedisTemplate<String,Bulletin> redisTemplate) {
		this.redisTemplate = redisTemplate;
	 }

	@Override
	public void put(Bulletin obj) {
		redisTemplate.opsForHash()
	    .put(obj.getObjectKey(), obj.getKey(), obj);
	}

	@Override
	public Bulletin get(Bulletin key) {
		return (Bulletin) redisTemplate.opsForHash().get(key.getObjectKey(),
			    key.getKey());
	}

	@Override
	public void delete(Bulletin key) {
		redisTemplate.opsForHash().delete(key.getObjectKey(), key.getKey());
		
	}

	@Override
	public List<Bulletin> getObjects() {
		List<Bulletin> bulletins = new ArrayList<Bulletin>();
		for (Object bulletin : redisTemplate.opsForHash().values(Bulletin.OBJECT_KEY) ){
			bulletins.add((Bulletin) bulletin);
		}
	return bulletins;
}

}
