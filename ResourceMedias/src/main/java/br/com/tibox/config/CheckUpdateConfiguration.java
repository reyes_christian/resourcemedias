package br.com.tibox.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import br.com.tibox.jobs.CheckUpdateJob;

@Configuration
@PropertySource("application.properties")
public class CheckUpdateConfiguration {

	@Value("${tibox.client.code}")
	private String clienteCode;
	
	@Value("${tibox.proxy.machineID}")
	private String machineId;
	
	@Value("${tibox.proxy.username}")
	private String userId;
	
	@Value("${tibox.proxy.password}")
	private String password;

	@Value("${tibox.proxy.historico.tamanho}")
	private Integer totalRegistro;
	
	@Value("${tibox.proxy.total.registros}")
	private Integer pagina;
	
	@Bean
	CheckUpdateJob CheckUpdateJob(){
		CheckUpdateJob checkJob = new CheckUpdateJob();
		
		checkJob.setClienteCode(clienteCode);
		checkJob.setMachineId(machineId);
		checkJob.setUserId(userId);
		checkJob.setPassword(password);
		checkJob.setPagina(pagina);
		checkJob.setTotalRegistro(totalRegistro);
		
		return checkJob;
	}
}
